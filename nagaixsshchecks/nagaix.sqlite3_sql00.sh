#!/bin/bash
# $Date$
# $Id$
# $Version: 0.3$
# $Revision: 5$
# $Author: Victor N. Skurikhin <V.Skurikhin@inform.gazprom.ru>$
################################################################################

data=nagaix.sqlite3
path=/var/spool/sqlite3
sqlite3="sqlite3 $path/$data"
SQL="SELECT * FROM nagaix_values;"

cat <<EOSQL01 | tee >(cat >&2) | $sqlite3
.mode column
.width 8 5 4
.headers on
EXPLAIN QUERY PLAN
$SQL
EOSQL01

cat <<EOSQL02 | tee >(cat >&2) | $sqlite3

.mode column
.width 4 36 1 4 4 34 4
.headers on
$SQL
EOSQL02

SQL="SELECT * FROM nagaix_services;"

cat <<EOSQL03 | tee >(cat >&2) | $sqlite3
.mode column
.width 8 5 4
.headers on
EXPLAIN QUERY PLAN
$SQL
EOSQL03

cat <<EOSQL04 | tee >(date 2>/dev/null 1>&2;cat >&2) | (date 1>&2;$sqlite3)

.mode column
.width 2 7 8 14 5
.headers on
$SQL
EOSQL04
