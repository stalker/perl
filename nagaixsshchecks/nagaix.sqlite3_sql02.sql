-- $Date$
-- $Id$
-- $Version: 0.2$
-- $Revision: 2$
-- $Author: Victor N. Skurikhin <V.Skurikhin@inform.gazprom.ru>$

SELECT service.id, service.host_id,
       valsrvs.svalue  AS service,
       valip.svalue    AS ip,
       valsrpt.svalue  AS script
  FROM nagaix_services AS service,
       nagaix_values   AS valsrvs,
       nagaix_hosts    AS naghost,
       nagaix_values   AS valip,
       nagaix_values   AS valsrpt
 WHERE service.host_id = (
    SELECT h.id
      FROM nagaix_values AS v,
           nagaix_hosts  AS h
     WHERE v.svalue = '127.0.0.1'
       AND v.id = h.ip_vid
    )
   AND service.name_vid = valsrvs.id
   AND service.host_id  = naghost.id
   AND naghost.ip_vid   = valip.id
   AND service.rem_script_vid = valsrpt.id;
