#!/bin/bash
# $Date$
# $Id$
# $Version: 0.3$
# $Revision: 3$
# $Author: Victor N. Skurikhin <V.Skurikhin@inform.gazprom.ru>$
################################################################################

if [ "x$1" == "x" ] ; then
  echo "Usage: $0 ip"
  exit 1
fi

sqlite3="sqlite3 /var/spool/sqlite3/nagaix.sqlite3"
SQL="\
SELECT service.id, service.host_id,
       valsrvs.svalue  AS service,
       valip.svalue    AS ip,
       valsrpt.svalue  AS script
  FROM nagaix_services AS service,
       nagaix_values   AS valsrvs,
       nagaix_hosts    AS naghost,
       nagaix_values   AS valip,
       nagaix_values   AS valsrpt
 WHERE service.host_id = (
    SELECT h.id
      FROM nagaix_values AS v,
           nagaix_hosts  AS h
     WHERE v.svalue = '$1'
       AND v.id = h.ip_vid
    )
   AND service.name_vid = valsrvs.id
   AND service.host_id  = naghost.id
   AND naghost.ip_vid   = valip.id
   AND service.rem_script_vid = valsrpt.id;
"
cat <<EOSQL | tee >(cat >&2) | $sqlite3
.mode column
.width 8 5 4
.headers on
EXPLAIN QUERY PLAN
$SQL
EOSQL
cat <<EOSQL | tee >(cat >&2) | $sqlite3

.mode line nagaix_hosts
.mode line nagaix_values
.mode line nagaix_services
$SQL
EOSQL
