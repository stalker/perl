#!/bin/bash
# $Date$
# $Id$
# $Version: 0.1$
# $Revision: 1$
# $Author: Victor N. Skurikhin <V.Skurikhin@inform.gazprom.ru>$
################################################################################

data=nagaix.sqlite3
path=/var/spool/sqlite3
sqlite3="sqlite3 $path/$data"
SN=${0##*/}
SN=${SN%%.sh}

cat <<EOSQL | tee >(cat >&2) | $sqlite3
.mode column
.width 8 5 4
.headers on
EXPLAIN QUERY PLAN
`cat $SN.sql`
EOSQL
cat <<EOSQL | tee >(cat >&2) | $sqlite3

.mode line nagaix_hosts
.mode line nagaix_values
.mode line nagaix_services
`cat $SN.sql`
EOSQL
