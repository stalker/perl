#!/bin/bash
# $Date$
# $Id$
# $Version: 0.3$
# $Revision: 3$
# $Author: Victor N. Skurikhin <V.Skurikhin@inform.gazprom.ru>$
################################################################################

sqlite3="sqlite3 /var/spool/sqlite3/nagaix.sqlite3"
SQL="\
SELECT srvs.id         AS srvs_id,
       host.id         AS host_id,
       valhost.svalue  AS hostname,
       valsnam.id      AS service_id,
       valsnam.svalue  AS service_name,
       valrscn.id      AS script_id,
       valrscn.svalue  AS service_script 
  FROM nagaix_services AS srvs,
       nagaix_hosts    AS host,
       nagaix_values   AS valhost,
       nagaix_values   AS valsnam,
       nagaix_values   AS valrscn
 WHERE srvs.host_id = 1 AND host.name_vid = valhost.id
   AND srvs.name_vid = valsnam.id AND srvs.rem_script_vid = valrscn.id;
"
cat <<EOSQL | tee >(cat >&2) | $sqlite3
.mode column
.width 8 5 4
.headers on
EXPLAIN QUERY PLAN
$SQL
EOSQL
cat <<EOSQL | tee >(cat >&2) | $sqlite3

.mode line nagaix_hosts
.mode line nagaix_values
.mode line nagaix_services
$SQL
EOSQL
