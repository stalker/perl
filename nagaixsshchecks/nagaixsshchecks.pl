#!/usr/bin/perl
# $Date$
# $Id$
# $Version: 0.99.2$
# $Revision: 12$
# $Author: Victor N. Skurikhin <V.Skurikhin@inform.gazprom.ru>$
################################################################################

use DBI;
use strict;
use Socket;
use Getopt::Std;
use Getopt::Long;
use Digest::MD5 qw(md5_hex);

my $data = "nagaix.sqlite3";
my $path = "/var/spool/sqlite3";
my $length = 24;
my $create;
my $debug;
my $verbose;
my $syslog = undef;
my $dbversion = 1;
my $sCheckname = undef;
my $sHostname = undef;
my $sRemscript = undef;
my $iTimeout = 60;

sub add_host($$$);
sub add_service($$$$);
sub get_id_host_by_ip($$);
sub get_id_service_by_name_host_id($$$);
sub get_all_id_hosts($);
# TODO sub get_all_services_by_host_id($$$);
sub create_database($);
sub add_or_set_value($$$$);
sub insert_or_update_dbversion($);
sub is_ipaddress($);
sub or_usual($@);
sub or_debug($@);
sub or_verbose($@);

my $ip_num = qr/(\d|[0-1]?\d\d|2[0-4]\d|25[0-5])/;
my $ip_pat = qr/^($ip_num\.){3}$ip_num$/os;

GetOptions("create" => \$create,          # flag
           "debug" => \$debug,            # flag
           "length=i" => \$length,        # numeric
           "file=s" => \$data,            # string
           "path=s" => \$path,            # string
           "Name=s" => \$sCheckname,      # string
           "Hostname=s" => \$sHostname,   # string
           "Remscript=s" => \$sRemscript, # string
           "Timeout=i" => \$iTimeout,     # string
           "verbose" => \$verbose,        # flag
           "syslog" => \$syslog)          # flag
or die("Error in command line arguments\n");

if ($verbose) {
  print(or_verbose(" create:   on\n")) if ($create);
  print(or_verbose(" debug:    on\n")) if ($debug);
  print(or_verbose(" verbose:  on\n"));
  print(or_verbose(" database: %s\n", $data));
  print(or_verbose(" Checkname:%s\n", $sCheckname));
  print(or_verbose(" Hostname: %s", $sHostname));
  print(STDOUT " Good IP!") if (is_ipaddress($sHostname));
  print(or_verbose("\n"));
  print(or_verbose(" Remscript:%s\n", $sRemscript));
  print(or_verbose(" Timeout:  %d\n", $iTimeout));
}

my $sIP = undef;
if (is_ipaddress($sHostname)) {
  $sIP = $sHostname;
  if (not $sHostname = gethostbyaddr(inet_aton($sIP), AF_INET)) {
    $sHostname = $sIP; 
    warn or_verbose("Can't resolve $sIP: $!\n");
  }
  or_debug(" Resolv:   %-15s %s\n", $sIP, $sHostname);
} else {
  eval { $sIP = inet_ntoa(inet_aton($sHostname)); };
  or_debug(" Resolv:   %-15s %s\n", $sIP, $sHostname);
  die or_verbose("%d: Cant resolv name: %s\n", __LINE__, $sHostname)
  if (not $sIP);
}

my $driver = "SQLite";
my $dsn = "DBI:$driver:dbname=$path/$data";
my $userid = "";
my $password = "";
my $dbh = DBI->connect($dsn, $userid, $password, { RaiseError => 1 })
          or die or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
or_verbose(" Opened database successfully");
create_database(\$dbh) if ($create);
add_or_set_value(\$dbh, 'start', 'I', 1);
my $host_id = add_host(\$dbh, $sIP, $sHostname);
add_service(\$dbh, $sCheckname, $sRemscript, $host_id);
my $id = get_id_service_by_name_host_id(\$dbh, $sCheckname, $host_id);
printf(STDOUT "service id = %d\n", $id);
foreach my $ip_id (get_all_id_hosts(\$dbh)) {
  printf(STDOUT "host id = %d\n", $ip_id);
}
$dbh->disconnect();
exit(0);

################################################################################
# Добавление хоста
# входные параметры
# 1-й ссылка на обьект DBI handle
# 2-й ip-адресс не пустое значение
# 3-й имя хоста
# возвращает скалярный результат если не 0 всё ok
# результат id добавленной или существующей записи
sub add_host($$$) {
  my $rdbh = shift;
  my $ip = shift;
  my $host = shift;
  my $result = 0;
  my ($stmt_insert, $stmt_select);
  ##############################################################################
  # добавляем или обновляем значение(value) ip-адреса
  # в переменную $vid_ip заносим id записи значение(value)
  my $vid_ip = add_or_set_value($rdbh, 'ip'.md5_hex($ip), 'S', $ip);
  # добавляем или обновляем значение(value) имени хоста
  # в переменную $vid_host заносим id записи значение(value)
  my $vid_host = add_or_set_value($rdbh, 'ht'.md5_hex($host), 'S', $host);
  # Щаблон оператора для вставки хоста
  # вставляется пара ссылок(id) на значения(values) ip и имени хоста
  # OR IGNORE - отсекаем ошибку при конфликте уникальности ключа
  my $sSQL_Q_insert_host_fmt = <<"EOSQL";
  INSERT OR IGNORE INTO nagaix_hosts
  (ip_vid, name_vid, state)
  VALUES (%d, %d, %d)
EOSQL
  # Щаблон оператора получения id вставленного хоста
  my $sSQL_Q_select_id_host_fmt = <<"EOSQL";
  SELECT id FROM nagaix_hosts WHERE ip_vid=%d AND name_vid=%d;
EOSQL
  $stmt_insert = sprintf($sSQL_Q_insert_host_fmt,
                         $vid_ip, $vid_host, 0);
  or_debug("%d: \n%s\n", __LINE__, $stmt_insert);
  my $rv = $$rdbh->do($stmt_insert)
  or die or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  # Проверяем есть ли значение которое мы пытались вставить
  # подготавливаем операцию получения
  return get_id_host_by_ip($rdbh, $ip);
}

################################################################################
# Добавление сервиса
sub add_service($$$$) {
  my $rdbh = shift;
  my $name = shift;
  my $rem_script = shift;
  my $host_id = shift;
  my $result = 0;
  my $sn = sprintf("sn%s", md5_hex($host_id.$name));
  my $vid_name = add_or_set_value($rdbh, $sn, 'S', $name);
  $sn = sprintf("rs%s", md5_hex($host_id.$name));
  my $vid_rem_script = add_or_set_value($rdbh,
                       $sn, 'S', $rem_script);
  my ($stmt_insert, $stmt_select);
  my $sSQL_Q_insert_host_fmt = <<"EOSQL";
  INSERT OR IGNORE INTO nagaix_services
  (host_id, name_vid, rem_script_vid, state)
  VALUES (%d, %d, %d, %d)
EOSQL
  my $sSQL_Q_select_id_host_fmt = <<"EOSQL";
  SELECT id FROM nagaix_services
   WHERE host_id=%d AND name_vid=%d AND rem_script_vid=%d;
EOSQL
  $stmt_insert = sprintf($sSQL_Q_insert_host_fmt, $host_id,
                         $vid_name, $vid_rem_script, 0);
  or_debug("%d: \n%s\n", __LINE__, $stmt_insert);
  my $rv = $$rdbh->do($stmt_insert)
  or die or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  # Подготавливаем операцию получения id значения(value)
  $stmt_select = sprintf($sSQL_Q_select_id_host_fmt,
                         $host_id, $vid_name, $vid_rem_script);
  my $sth = $$rdbh->prepare( $stmt_select );
  $rv = $sth->execute()
  or die or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  if ($rv < 0) {
    or_debug("%d:Error:%s", __LINE__, $DBI::errstr)
    or or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  }
  or_debug("%d: \n%s\n", __LINE__, $stmt_select);
  while (my @row = $sth->fetchrow_array()) {
    or_debug("%d: id = ".$row[0], __LINE__);
    $result = $row[0];
  }
  $result = 0 if ($result !~ /^\d+$/);
  return $result;
}

sub get_id_host_by_ip($$) {
  my $rdbh = shift;
  my $ip = shift;
  return undef if (not is_ipaddress($ip));
  my $result = 0;
  my $stmt_select;
  my $sSQL_Q_select_id_host_fmt = <<"EOSQL";
  SELECT h.id FROM nagaix_values AS v,
                   nagaix_hosts AS h
   WHERE v.svalue = "%s"
     AND v.id = h.ip_vid;
EOSQL
  # Подготавливаем операцию получения id значения(value)
  $stmt_select = sprintf($sSQL_Q_select_id_host_fmt, $ip);
  my $sth = $$rdbh->prepare( $stmt_select );
  my $rv = $sth->execute()
  or die or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  if ($rv < 0) {
    or_debug("%d:Error:%s", __LINE__, $DBI::errstr)
    or or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  }
  or_debug("%d: \n%s\n", __LINE__, $stmt_select);
  while (my @row = $sth->fetchrow_array()) {
    or_debug("%d: id = ".$row[0], __LINE__);
    $result = $row[0];
  }
  $result = 0 if ($result !~ /^\d+$/);
  return $result;

  #  $stmt_select = sprintf($sSQL_Q_select_id_host_fmt,
  #                         $vid_ip, $vid_host);
  #  my $sth = $$rdbh->prepare( $stmt_select );
  #  $rv = $sth->execute()
  #  or die or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  #  if ($rv < 0) {
  #    or_debug("%d:Error:%s", __LINE__, $DBI::errstr)
  #    or or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  #  }
  #  or_debug("%d: \n%s\n", __LINE__, $stmt_select);
  #  while (my @row = $sth->fetchrow_array()) {
  #    or_debug("%d: id = ".$row[0], __LINE__);
  #    $result = $row[0];
  #  }
  #  $result = 0 if ($result !~ /^\d+$/);
  #  return $result;
}

sub get_id_service_by_name_host_id($$$) {
  my $rdbh = shift;
  my $name = shift;
  my $host_id = shift;
  return undef if (not $name or not $host_id);
  my ($type) = (@_);
  $type = 'S' if (not $type or $type !~ /^(I|R|T)$/);
  my $result = 0;
  my $sn = sprintf("sn%s", md5_hex($host_id.$name));


  my $sSQL_Q_select_id_host_fmt = <<"EOSQL";
  SELECT id FROM nagaix_services
   WHERE host_id = %d
     AND name_vid = (
       SELECT id FROM nagaix_values
       WHERE name = '%s' AND type = '%s'
     )
EOSQL

  # Подготавливаем операцию получения id значения(value)
  my $stmt_select = sprintf($sSQL_Q_select_id_host_fmt,
                            $host_id, $sn, $type);
  my $sth = $$rdbh->prepare( $stmt_select );
  my $rv = $sth->execute()
  or die or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  if ($rv < 0) {
    or_debug("%d:Error:%s", __LINE__, $DBI::errstr)
    or or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  }
  or_debug("%d: \n%s\n", __LINE__, $stmt_select);
  while (my @row = $sth->fetchrow_array()) {
    or_debug("%d: id = ".$row[0], __LINE__);
    $result = $row[0];
  }
  $result = 0 if ($result !~ /^\d+$/);
  return $result;
}

sub get_all_id_hosts($) {
  my $rdbh = shift;
  my @result = undef;
  my $sSQL_Q_select_id_host_fmt = <<"EOSQL";
    SELECT id FROM nagaix_hosts;
EOSQL
  # Подготавливаем операцию получения id значения(value)
  my $sth = $$rdbh->prepare($sSQL_Q_select_id_host_fmt);
  my $rv = $sth->execute()
  or die or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  if ($rv < 0) {
    or_debug("%d:Error:%s", __LINE__, $DBI::errstr)
    or or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  }
  or_debug("%d: \n%s\n", __LINE__, $sSQL_Q_select_id_host_fmt);
  while (my @row = $sth->fetchrow_array()) {
    or_debug("%d: id = ".$row[0], __LINE__);
    push(@result, $row[0]) if ($row[0] =~ /^\d+$/);
  }
  return @result;
}

################################################################################
# Создание БД
# входной параметр ссылка на обьект DBI handle
# возвращает скалярный результат если не 0 всё ok
sub create_database($) {
  my $rdbh = shift;
  my @aListSQL;

  ##############################################################################
  # Создание таблицы nagaix_dbversion
  # содержит одну запись которая хранит версию схемы
  my $sObject_name = 'nagaix_dbversion';
  my $sSQL_DQL_create_table_dbversion = <<"EOSQL";
  CREATE TABLE $sObject_name ( version INTEGER,
    id INTEGER PRIMARY KEY AUTOINCREMENT );
EOSQL
  push(@aListSQL, { name => $sObject_name,
    sql => $sSQL_DQL_create_table_dbversion,
    type => 'Table'
  });

  ##############################################################################
  # Создание таблицы nagaix_values
  # хранилище значений(value)
  # name - имя значения(value)
  # type - тип значения(value)
  # в зависимости от типа значение(value) сохраняется в одно из полей:
  # ivalue, rvalue, svalue или tvalue
  # timestamp - время вставки или обновления
  $sObject_name = 'nagaix_values';
  my $sSQL_DQL_create_table_values = <<"EOSQL";
  CREATE TABLE $sObject_name
  ( id INTEGER PRIMARY KEY AUTOINCREMENT,
    name           CHAR(36)NOT NULL,
    type           CHAR(1) NOT NULL,
    ivalue         INTEGER,
    rvalue         REAL,
    svalue         CHAR(255),
    tvalue         TEXT,
    timestamp      DATETIME DEFAULT CURRENT_TIMESTAMP );
EOSQL
  push(@aListSQL, { name => $sObject_name,
    sql => $sSQL_DQL_create_table_values,
    type => 'Table'
  });

  ##############################################################################
  # Создание индекса для таблицы nagaix_values
  # индекс уникальный включает имя и тип
  $sObject_name = 'nagaix_values_nt';
  my $sSQL_DQL_create_index_nt = <<"EOSQL";
  CREATE UNIQUE INDEX $sObject_name ON nagaix_values
         (name ASC, type ASC);
EOSQL
  push(@aListSQL, { name => $sObject_name,
      sql => $sSQL_DQL_create_index_nt,
      type => 'Index'
  });

  ##############################################################################
  # Создание таблицы nagaix_hosts
  # содержит ip и имя хоста
  # ip_vid    - id ip-ка
  # name_vid  - id имени хоста
  # state     - статус хоста
  # timestamp - время вставки или обновления
  $sObject_name = 'nagaix_hosts';
  my $sSQL_DQL_create_table_hosts = <<"EOSQL";
  CREATE TABLE $sObject_name
  ( id INTEGER PRIMARY KEY AUTOINCREMENT,
    ip_vid REFERENCES nagaix_values(id),
    name_vid REFERENCES nagaix_values(id),
    state INTEGER NOT NULL,
    TIMESTAMP DATETIME DEFAULT CURRENT_TIMESTAMP );
EOSQL
  push(@aListSQL, { name => $sObject_name,
      sql => $sSQL_DQL_create_table_hosts,
      type => 'Table'
  });

  ##############################################################################
  # Создание индекса для таблицы nagaix_hosts
  # индекс уникальный включает ip
  $sObject_name = 'nagaix_hosts_idx_ip';
  my $sSQL_DQL_create_index_ip = <<"EOSQL";
  CREATE UNIQUE INDEX $sObject_name ON nagaix_hosts
         (ip_vid ASC);
EOSQL
  push(@aListSQL, { name => $sObject_name,
      sql => $sSQL_DQL_create_index_ip,
      type => 'Index'
  });

  ##############################################################################
  # Создание индекса для таблицы nagaix_hosts
  # индекс включает host
  $sObject_name = 'nagaix_hosts_idx_h';
  my $sSQL_DQL_create_index_h = <<"EOSQL";
  CREATE INDEX $sObject_name ON nagaix_hosts
         (id ASC, name_vid ASC);
EOSQL
  push(@aListSQL, { name => $sObject_name,
      sql => $sSQL_DQL_create_index_h,
      type => 'Index'
  });

  ##############################################################################
  # Создание таблицы nagaix_services
  # содержит хост, имя сервиса и имя удалённого скрипта
  # host_id        - id хоста(в таблице nagaix_hosts)
  # name_vid       - id имени(value) сервиса
  # rem_script_vid - id имени(value) скрипта
  # state          - статус сервиса
  # timestamp      - время вставки или обновления
  $sObject_name = 'nagaix_services';
  my $sSQL_DQL_create_table_services = <<"EOSQL";
  CREATE TABLE $sObject_name
  ( id INTEGER PRIMARY KEY AUTOINCREMENT,
    host_id REFERENCES nagaix_hosts(id),
    name_vid REFERENCES nagaix_values(id),
    rem_script_vid REFERENCES nagaix_values(id),
    state INTEGER NOT NULL,
    TIMESTAMP DATETIME DEFAULT CURRENT_TIMESTAMP );
EOSQL
  push(@aListSQL, { name => $sObject_name,
      sql => $sSQL_DQL_create_table_services,
      type => 'Table'
  });

  ##############################################################################
  # Создание индекса для таблицы nagaix_services
  # индекс уникальный включает host_id и name_vid
  $sObject_name = 'nagaix_service_idx_hn';
  my $sSQL_DQL_create_index_service_hn = <<"EOSQL";
  CREATE UNIQUE INDEX $sObject_name ON nagaix_services
         (host_id ASC, name_vid ASC);
EOSQL
  push(@aListSQL, { name => $sObject_name,
      sql => $sSQL_DQL_create_index_service_hn,
      type => 'Index'
  });

  ##############################################################################
  # Создание индекса для таблицы nagaix_services
  # индекс включает rem_script_vid
  $sObject_name = 'nagaix_service_idx_rs';
  my $sSQL_DQL_create_index_service_rs = <<"EOSQL";
  CREATE INDEX $sObject_name ON nagaix_services
         (id ASC, rem_script_vid ASC);
EOSQL
  push(@aListSQL, { name => $sObject_name,
      sql => $sSQL_DQL_create_index_service_rs,
      type => 'Index'
  });

  ##############################################################################
  # Создание триггера на удаление для таблицы nagaix_values
  $sObject_name = 'nagaix_values_on_delete_set_zero';
  my $sSQL_DQL_create_trigger_nagaix_values_on_delete = <<"EOSQL";
  CREATE TRIGGER $sObject_name
    AFTER DELETE ON nagaix_values BEGIN
    UPDATE nagaix_hosts SET ip_vid = 0 WHERE ip_vid = old.id;
    UPDATE nagaix_hosts SET name_vid = 0 WHERE name_vid = old.id;
    UPDATE nagaix_services SET name_vid = 0 WHERE name_vid = old.id;
    UPDATE nagaix_services SET rem_script_vid = 0 WHERE rem_script_vid = old.id;
  END;
EOSQL
  push(@aListSQL, { name => $sObject_name,
      sql => $sSQL_DQL_create_trigger_nagaix_values_on_delete,
      type => 'Trigger'
  });

  ##############################################################################
  # Создание триггера на обновление для таблицы nagaix_values
  $sObject_name = 'nagaix_values_on_update';
  my $sSQL_DQL_create_trigger_nagaix_values_on_update = <<"EOSQL";
  CREATE TRIGGER $sObject_name
    AFTER UPDATE ON nagaix_values BEGIN
    UPDATE nagaix_hosts SET ip_vid = new.id WHERE ip_vid = old.id;
    UPDATE nagaix_hosts SET name_vid = new.id WHERE name_vid = old.id;
    UPDATE nagaix_services SET name_vid = new.id WHERE name_vid = old.id;
    UPDATE nagaix_services SET rem_script_vid = new.id
     WHERE rem_script_vid = old.id;
  END;
EOSQL
  push(@aListSQL, { name => $sObject_name,
      sql => $sSQL_DQL_create_trigger_nagaix_values_on_update,
      type => 'Trigger'
  });

  ##############################################################################
  # Создание триггера на обновление для таблицы nagaix_hosts
  $sObject_name = 'nagaix_hosts_on_update';
  my $sSQL_DQL_create_trigger_nagaix_hosts_on_update = <<"EOSQL";
  CREATE TRIGGER $sObject_name
    AFTER UPDATE ON nagaix_hosts BEGIN
    UPDATE nagaix_services SET host_id = new.id WHERE host_id = old.id;
  END;
EOSQL
  push(@aListSQL, { name => $sObject_name,
      sql => $sSQL_DQL_create_trigger_nagaix_hosts_on_update,
      type => 'Trigger'
  });

  ##############################################################################
  # Создание триггера на удаление для таблицы nagaix_hosts
  $sObject_name = 'nagaix_hosts_on_delete_deleting';
  my $sSQL_DQL_create_trigger_nagaix_hosts_on_delete = <<"EOSQL";
  CREATE TRIGGER $sObject_name
    AFTER DELETE ON nagaix_hosts BEGIN
    DELETE FROM nagaix_services WHERE host_id = old.id;
  END;
EOSQL
  push(@aListSQL, { name => $sObject_name,
      sql => $sSQL_DQL_create_trigger_nagaix_hosts_on_delete,
      type => 'Trigger'
  });

  $$rdbh->do("PRAGMA foreign_keys = ON");
  foreach my $e (@aListSQL) {
    or_debug("%d name: %s\n", __LINE__, $e->{name});
    or_debug("%d sql: \n%s\n", __LINE__, $e->{sql});
    or_debug("%d type: %s\n", __LINE__, $e->{type});
    my $rv = $$rdbh->do($e->{sql});
    if ($rv < 0) {
      or_debug("%d:Error:%s", __LINE__, $DBI::errstr)
      or or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
    } else {
      or_verbose(" %s '%s' created successfully\n", $e->{type}, $e->{name})
      or or_usual(" %s '%s' created successfully\n", $e->{type}, $e->{name});
    }
  }
  insert_or_update_dbversion($rdbh);
  return 0;
}

################################################################################
# Вставка или обновление заничения(value) в таблице nagaix_values
# входные параметры
# 1-й ссылка на обьект DBI handle
# 2-й имя значения(value)
# 3-й тип значения(value)
# 4-й значение значения(value)
# возвращает скалярный результат если не 0 всё ok
# результат id добавленной/обработанной записи
sub add_or_set_value($$$$) {
  my $rdbh = shift;
  my $sTable_NAME = shift;
  my $sType = shift;
  # обработка типа, только один символ в верхнем регистре
  # I - INTEGER
  # R - REAL
  # S - CHAR(255)
  # T - TEXT
  $sType =~ s/^(.).*$/$1/;
  my $sTable_TYPE = uc($sType);
  my $sType_lower = lc($sType);
  my $sTable_VALUE = shift;
  my $result = 0;
  my $range = "'";
  our ($stmt_insert, $stmt_update, $stmt_select);
  # Щаблон оператора для вставки значения(value)
  # OR IGNORE - отсекаем ошибку при конфликте уникальности ключа
  my $sSQL_Q_insert_value_fmt = <<"EOSQL";
  INSERT OR IGNORE INTO nagaix_values
  (name, type, %svalue)
  VALUES ('%s', '%s', %s%s%s)
EOSQL
  # Щаблон оператора для обновления значения(value)
  my $sSQL_Q_update_value_fmt = <<"EOSQL";
  UPDATE nagaix_values
  SET %svalue=%s%s%s, timestamp=DATETIME('now')
  WHERE name='%s' AND type='%s';
EOSQL
  # Щаблон оператора получения id вставленного/обновлённого значения(value)
  my $sSQL_Q_select_id_value_fmt = <<"EOSQL";
  SELECT id FROM nagaix_values WHERE name='%s' AND type='%s';
EOSQL
  if ($sTable_TYPE !~ /^I|R|S$/ ) {
    $sTable_TYPE = $sType = 'T';
    $sType_lower = lc($sType);
  }
  $range = "" if ('I' eq $sTable_TYPE);
  # Подготавливаем операцию вставки
  $stmt_insert = sprintf($sSQL_Q_insert_value_fmt,
                         $sType_lower,
                         $sTable_NAME,
                         $sTable_TYPE,
                         $range, $sTable_VALUE, $range);
  # Подготавливаем операцию обновления
  $stmt_update = sprintf($sSQL_Q_update_value_fmt,
                         $sType_lower,
                         $range, $sTable_VALUE, $range,
                         $sTable_NAME,
                         $sTable_TYPE);
  # Подготавливаем операцию получения id значения(value)
  $stmt_select = sprintf($sSQL_Q_select_id_value_fmt,
                         $sTable_NAME,
                         $sTable_TYPE);
  or_debug("%d: \n%s\n", __LINE__, $stmt_insert);
  # попытка вставить значение
  # если есть значиние с таким же именем и типом
  my $rv = $$rdbh->do($stmt_insert)
  or die or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  or_debug("%d: \n%s\n", __LINE__, $stmt_update);
  $rv = $$rdbh->do($stmt_update)
  or warn or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  or_verbose(" Records created successfully\n") if ($verbose);
  my $sth = $$rdbh->prepare( $stmt_select );
  $rv = $sth->execute()
  or die or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  if ($rv < 0) {
    or_debug("%d:Error:%s", __LINE__, $DBI::errstr)
    or or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  }
  or_debug("%d: \n%s\n", __LINE__, $stmt_select);
  while (my @row = $sth->fetchrow_array()) {
    or_debug("%d: id = ".$row[0], __LINE__);
    $result = $row[0];
  }
  $result = 0 if ($result !~ /^\d+$/);
  return $result;
}

sub insert_or_update_dbversion($) {
  my $rdbh = shift;
  my $result = 0;
  my $stmt_insert = <<"EOSQL";
  INSERT OR IGNORE INTO nagaix_dbversion
  ( version, id ) VALUES ( $dbversion, 1 )
EOSQL
  my $stmt_update = <<"EOSQL";
  UPDATE nagaix_dbversion
  SET version = $dbversion
  WHERE id = 1;
EOSQL
  my $stmt_select = <<"EOSQL";
  SELECT version FROM nagaix_dbversion WHERE id = 1;
EOSQL
  or_debug("%d: \n%s\n", __LINE__, $stmt_insert);
  my $rv = $$rdbh->do($stmt_insert)
  or die or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  or_debug("%d: \n%s\n", __LINE__, $stmt_update);
  $rv = $$rdbh->do($stmt_update)
  or warn or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  or_verbose(" Records created successfully\n") if ($verbose);
  my $sth = $$rdbh->prepare( $stmt_select );
  $rv = $sth->execute()
  or die or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  if ($rv < 0) {
    or_debug("%d:Error:%s", __LINE__, $DBI::errstr)
    or or_usual("%d:Error:%s", __LINE__, $DBI::errstr);
  }
  or_debug("%d: \n%s\n", __LINE__, $stmt_select);
  while (my @row = $sth->fetchrow_array()) {
    or_debug("%d: id = ".$row[0], __LINE__);
    $result = $row[0];
  }
  $result = 0 if ($result !~ /^\d+$/);
  return $result;
}

sub is_ipaddress($) {
  my $ip = shift;
  return $ip if ($ip =~ /$ip_pat/);
  return undef;
}

sub or_usual($@) {
  if (!($debug || $verbose)) {
    my $fmt = shift;
    if ($syslog) {
      use Unix::Syslog qw(:macros :subs);
      map { s/\n//gs } [ @_ ];
      syslog(LOG_INFO, "%s:".$fmt, __FILE__, @_);
    }
    return sprintf("%s:".$fmt, __FILE__, @_);
  }
  return undef;
}

sub or_debug($@) {
  if ($debug) {
    my $fmt = shift;
    if ($syslog) {
      use Unix::Syslog qw(:macros :subs);
      map { s/\n//gs } [ @_ ];
      syslog(LOG_INFO, "%s:".$fmt, __FILE__, @_);
    }
    printf(STDERR "%s:".$fmt."\n", __FILE__, @_);
    return sprintf("%s:".$fmt, __FILE__, @_);
  }
  return undef;
}

sub or_verbose($@) {
  if ($verbose) {
    my $fmt = shift;
    if ($syslog) {
      use Unix::Syslog qw(:macros :subs);
      map { s/\n//gs } [ @_ ];
      syslog(LOG_INFO, "%s:".$fmt, __FILE__, @_);
    }
    return sprintf("%s:".$fmt, __FILE__, @_);
  }
  return undef;
}

__END__
################################################################################
# vim: syntax=perl:fileencoding=utf-8:fileformat=unix:tw=78:ts=2:sw=2:sts=2:et
# EOF
