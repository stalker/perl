#!/usr/bin/perl
# $Date$
# $Id$
# $Version: 0.2$
# $Revision: 2$
# $Author: Victor N. Skurikhin <V.Skurikhin@inform.gazprom.ru>$
my $lambda;
my $m = '#';
my $trigger = 1;
if ('-sql' eq $ARGV[0]) {
  $m = '--';
  shift @ARGV;
}
my $r0 = qr/^$m/os;
if ($0 =~ /_clean_/) {
  my $r1 = qr/^($m \$Date): .* \d\d:\d\d:\d\d\$/os;
  $lambda = sub { s/$r1/\1\$/; };
} else {
  use POSIX qw(LC_ALL setlocale strftime);
  setlocale( &LC_ALL, "C" );
  my $now = strftime("%Y-%m-%d %H:%M:%S", localtime);
  my $r1 = qr/^($m \$Date):?.*\$/os;
  $lambda = sub { s/$r1/\1: $now\$/; };
}
sub prep {
  if (/$r0/) { &$lambda; }
  else { $trigger = undef; }
  print;
}
while (<>) {
  if ($trigger) { prep; } else { print; }
}
# vim: syntax=perl:fileencoding=utf-8:fileformat=unix:tw=78:ts=2:sw=2:sts=2:et
#$ git config --global --replace-all filter.expand_date_smudge_pl.smudge expand_date_smudge_sql.pl
#$ git config --global --replace-all filter.expand_date_smudge_pl.clean expand_date_clean_sql.pl
#$ git config --global --replace-all filter.expand_date_smudge_sql.clean "expand_date_clean_sql.pl -sql"
#$ git config --global --replace-all filter.expand_date_smudge_sql.smudge "expand_date_smudge_sql.pl -sql"
