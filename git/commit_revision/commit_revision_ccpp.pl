#!/usr/bin/perl
# $Date$
# $Id$
# $Version: 0.5$
# $Revision: 5$
# $Author: Victor N. Skurikhin <V.Skurikhin@inform.gazprom.ru>$

use File::Copy;

my $debug = 1;
my @files;

while (@ARGV) {
  for my $fn (shift @ARGV) {
    my $ch = 0;
    open(GIT, "git diff --cached -- $fn|")
      or die "Git run failed for file $fn $!";
    while (<GIT>) {
      ++$ch if (not /^$/);
    }
    close(GIT);
    printf(STDERR "PID:%d FILE:%s \$ch:%d\n", $$, $fn, $ch)
    if ($debug > 0);
    if (0 < $ch) {
    my $r = 0;
    printf(STDERR "PID:%d FILE:%s\n", $$, $fn)
    if ($debug > 0);
    open(FIH, "< ".$fn) or die "Open failed for file $fn $!";
    WHILE: while (readline(FIH)) {
       if (/^\/\*/ or /^ \*/) {
         print;
         ++$r if (/^ \* \$Revision: \d+\$/);
       } else {
         last WHILE;
       }
    }
    close($fh);
    push(@files, $fn) if ($r > 0);
    }
  }
}

my @tmpfiles;

for my $fn (@files) {
  my $rev = 0;
  open(GIT, "git log -- $fn|") or die "Git run failed for file $fn $!";
  while (<GIT>) {
    ++$rev if (/^commit/);
  }
  close(GIT);
  my $tmpfile = "$fn.commit_revision.$$";
  copy($fn, $tmpfile) or die "Copy failed for file $fn: $!";
  open(FIH, "< ".$tmpfile) or die "Open failed for file $tmpfile $!";
  open(FOH, "> ".$fn) or die "Open failed for file $fn $!";
  WHILE: while (readline(FIH)) {
    if (/^ \* \$Revision: (\d+)\$/) {
      my $Revision = $1 + 1;
      $Revision = $rev if ($Revision < ($rev + 1));
      s/^( \* \$Revision: )(\d+)\$/\1$Revision\$/;
      printf(STDERR "PID:%d FILE:%s \$Revision:%d \$rev:%d\n",
                         $$, $fn, $Revision, $rev)
      if ($debug > 0);
    }
    print(FOH $_);
  }
  close(FOH);
  close(FIH);
  push(@tmpfiles, $tmpfile);
  `git add $fn`;
}

unlink(@tmpfiles);

exit(0);
# vim: syntax=perl:fileencoding=utf-8:fileformat=unix:tw=78:ts=2:sw=2:sts=2:et
__DATA__
