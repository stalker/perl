#!/usr/bin/perl
# $Date$
# $Id$
# $Version: 0.7$
# $Revision: 5$
# $Author: Victor (Stalker) Skurikhin <stalker@quake.ru>$

# Mysql external auth script
# Features: auth isUser and change password
# Password is an encrypted password !!

# 2014-02-11 - by Victor Skurikhin - http://jabber.la

# Settings
my $sDomain = 'jabber';
my $sDatabaseHost='127.0.0.1';    # The hostname of the database server
my $sDatabaseUser="USERNAME";     # The username to connect to mysql
my $sDatabasePass='PASSWORD';     # The password to connect to mysql
# The name of the database contain the user table
my $sDatabaseName="DATABASENAME";
# The name of the table containing the username and password
my $sUserTable="users";
# The name of the field that holds jabber user names
my $sUsernameTableField="username";
# The name of the field that holds jabber passwords
my $sPasswordTableField="bpassword";
my $sDSN = 'DBI:mysql:database='.$sDatabaseName.';host='.$sDatabaseHost;
my $odbh;

my $sSQL_Q_auth = <<"EOSQL";
SELECT AES_DECRYPT($sPasswordTableField, ?) AS $sPasswordTableField
  FROM $sUserTable
 WHERE $sUsernameTableField=?;
EOSQL

my $sSQL_Q_SetPass = <<"EOSQL";
UPDATE $sUserTable
   SET $sPasswordTableField=AES_ENCRYPT(?, ?)
 WHERE $sUsernameTableField=?;
EOSQL

my $sSQL_Q_TryRegister = <<"EOSQL";
INSERT INTO $sUserTable VALUES (?, '', NULL, AES_ENCRYPT(?, ?));
EOSQL

my $sSQL_Q_IsUser = <<"EOSQL";
SELECT count(*) AS iCount FROM $sUserTable WHERE $sUsernameTableField=?;
EOSQL

my $sSQL_Q_InfSchemaUserName = <<"EOSQL";
SELECT table_name
  FROM information_schema.columns
 WHERE table_schema = '$sDatabaseName'
   AND column_name = '$sUsernameTableField'
EOSQL

# Libs
use DBI;
use DBD::mysql;
use Unix::Syslog qw(:macros :subs);
use Digest::SHA qw(sha256_hex);

my %hAttr = (
  PrintError => 0,
  RaiseError => 0,
);

my $sInt_Sig = "PID: $$, int_handler ";
my $sAlrm_Sig = "PID: $$, alrm_handler ";
my $sQuit_Sig = "PID: $$, quit_handler ";

sub int_handler {
  syslog(LOG_INFO, $sInt_Sig);
  exit(1);
}

sub alrm_handler {
  syslog(LOG_INFO, $sAlrm_Sig);
  exit(2);
}

sub quit_handler {
  syslog(LOG_INFO, $sQuit_Sig);
  exit(3);
}

sub warn_handler {
  my ($wrn) = @_;
  syslog(LOG_INFO, "PID: $$, WARN[%s]", $wrn);
}

$SIG{INT} = \&int_handler;
$SIG{ALRM} = \&alrm_handler;
$SIG{QUIT} = \&quit_handler;
$SIG{__WARN__} = \&warn_handler;

sub isuser($$$$;@) {
  my $bResult = 0;
  my $rdbh = shift;
  my $rsth = shift;
  my $sQuery = shift;
  my $sValue = shift;
  syslog(LOG_INFO, "PID: %d, sub isuser (\$dbh = %s)", $$, $$rdbh);
  syslog(LOG_INFO, "PID: %d, sub isuser (\$sQuery = %s)", $$, $sQuery);
  syslog(LOG_INFO, "PID: %d, sub isuser (\$sValue = %s)", $$, $sValue);
  my $iParam_Pos = 1;
  $$rsth = $$rdbh->prepare($sQuery);
  foreach my $sParam (@_) {
    $$rsth->bind_param($iParam_Pos, $sParam);
    syslog(LOG_INFO, "PID: %d, sub isuser (\$iParam_Pos = %d, \$sParam = %s)",
           $$, $iParam_Pos, $sParam);
    $iParam_Pos += 1;
  }
  $$rsth->execute();
  my $hRow = $$rsth->fetchrow_hashref();
  if ($hRow->{$sValue} >= 1) {
    syslog(LOG_INFO, "PID: %d, sub isuser (%s)", $$, $sValue);
    $bResult = 1;
  }
  return wantarray ? ($bResult, $$rsth) : $bResult;
}

sub reg_or_update_user($$$$;@) {
  my $bResult = 0;
  my $rdbh = shift;
  my $rsth = shift;
  my $sQuery = shift;
  my $sOperation = shift;
  syslog(LOG_INFO, "PID: %d, sub reg_or_update_user (\$dbh = %s)",
         $$, $$rdbh);
  syslog(LOG_INFO, "PID: %d, sub reg_or_update_user (\$sQuery = %s)",
         $$, $sQuery);
  syslog(LOG_INFO, "PID: %d, sub reg_or_update_user (\$sOperation = %s)",
         $$, $sOperation);
  my $iParam_Pos = 1;
  my $bOldAC     = $$rdbh->{AutoCommit};
  $$rdbh->{AutoCommit} = 0;
  $$rsth = $$rdbh->prepare($sQuery);
  eval {
    foreach my $sParam (@_) {
      $$rsth->bind_param($iParam_Pos, $sParam);
      syslog(LOG_INFO, "PID: %d, sub reg_or_update_user"
             ." (\$sOperation = %s, \$iParam_Pos = %d, \$sParam = %s)",
             $$, $sOperation, $iParam_Pos, $sParam);
      $iParam_Pos += 1;
    }
    $$rsth->execute();
    $$rdbh->commit();
    $bResult = 1;
  };
  if ($@) {
    syslog(LOG_INFO, "PID: %d, reg_or_update_user (\$sOperation = %s)"
           ." DBI::errstr: %s; \$@: %s.",
           $$, $sOperation, $DBI::errstr, $@);
    eval { $$rdbh->rollback(); };
  }
  $$rdbh->{AutoCommit} = $bOldAC;
  return wantarray ? ($bResult, $$rsth) : $bResult;
}

sub removeuser3($$$) {
  my $bResult = 0;
  my $rdbh = shift;
  my $sOperation = shift;
  my $sUsername = shift;
  syslog(LOG_INFO, "PID: %d, sub reg_or_update_user (\$dbh = %s)",
         $$, $$rdbh);
  syslog(LOG_INFO, "PID: %d, sub reg_or_update_user (\$sOperation = %s)",
         $$, $sOperation);
  # $$rdbh->{RaiseError} = 1;
  eval {
    my $osth_Tables = $$rdbh->prepare($sSQL_Q_InfSchemaUserName)
    or warn $$rdbh->errstr;
    $osth_Tables->execute();
    my $raTables = $osth_Tables->fetchall_arrayref;
    foreach my $rTable (@$raTables) {
      syslog(LOG_INFO, "PID: %d, sub reg_or_update_user"
            . " (\$sOperation = %s, \$sTable = %s, \$sUsername = %s)"
            , $$, $sOperation, $$rTable[0], $sUsername);
      my $sSQL_Q_RemoveUser = <<"EOSQL";
         DELETE IGNORE FROM $$rTable[0] WHERE $sUsernameTableField='$sUsername';
EOSQL
      $$rdbh->do($sSQL_Q_RemoveUser)
      or warn $$rdbh->errstr;
    }
    $bResult = 1;
  };
  return $bResult;
}

while (1) {
  my ($sQuery, $osth_Auth, $osth_SetPass, $osth_TryRegister, $osth_IsUser);
  ### Попытка соединения с базой данных. Если соединиться не удолось,
  ### сделать паузу и повторить попытку
  until ($odbh = DBI->connect($sDSN, $sDatabaseUser, $sDatabasePass, \%hAttr)) {
    syslog(LOG_INFO, "PID: %d, Cant connect: %s, sleep for 5 min.",
           $$, $DBI::errstr);
    sleep(5 * 60);
  }
  syslog(LOG_INFO, "PID: %d, dbh (%s)", $$, $odbh);
  eval { ### Перехват _всех_ отказов внутри кода
    ### Установить автоматический контроль ошибок
    ### для дескриптора базы данных
    $odbh->{RaiseError} = 1;

    while (1) {
      syslog(LOG_INFO, "PID: %d, waiting for packet", $$);

      my $sBuffer       = "";
      my $readBuffer    = sysread(STDIN, $sBuffer, 2);
      do { 
        syslog(LOG_INFO, "PID: $$, port closed");
        exit;
      } unless $readBuffer == 2;
      my $iBufferLength = unpack("n", $sBuffer);
      my $readBuffer    = sysread(STDIN, $sBuffer, $iBufferLength);
      my ($sOperation, $sUsername, $sDomain, $sPassword) = split(/:/, $sBuffer);
      my $sSHA256hex    = sha256_hex($sUsername.$sDomain);
      my $bResult = 2;

      syslog(LOG_INFO, "PID: %d, request (%s => %s:%s:%s)",
             $$, $sOperation, $sUsername, $sDomain, $sPassword);

      SWITCH: {
        $sOperation eq 'auth' and do {
          $bResult   = 0;
          $sQuery    = $sSQL_Q_auth;
          $osth_Auth = $odbh->prepare($sQuery);
          $osth_Auth->bind_param(1, $sSHA256hex);
          $osth_Auth->bind_param(2, $sUsername);
          $osth_Auth->execute();
          while ($row = $osth_Auth->fetchrow_hashref()) {
            syslog(LOG_INFO, "PID: %d, auth (%s => %s)", $$,
                   $sUsername, $row->{$sPasswordTableField});
            if ($row->{$sPasswordTableField} eq $sPassword) {
              syslog(LOG_INFO, "PID: %d, auth ok (%s => %s)", $$,
                     $sUsername, $row->{$sPasswordTableField});
              $bResult = 1;
            }
          }
        },last SWITCH;
 
        $sOperation eq 'setpass' and do {
          $bResult = 0;
          ($bResult, $osth_SetPass) =
            reg_or_update_user(\$odbh, \$osth_SetPass,
                              , $sSQL_Q_SetPass, $sOperation,
                              , $sPassword, $sSHA256hex
                              , $sUsername
          );
        },last SWITCH;

        $sOperation eq 'tryregister' and do {
          $bResult = 0;
          ($bResult, $osth_TryRegister) =
            reg_or_update_user(\$odbh, \$osth_TryRegister,
                              , $sSQL_Q_TryRegister, $sOperation,
                              , $sUsername, $sPassword
                              , $sSHA256hex
          );
        },last SWITCH;

        $sOperation eq 'isuser' and do {
          $bResult          = 0;
          ($bResult, $osth_IsUser) =
            isuser(\$odbh, \$osth_IsUser,
                   $sSQL_Q_IsUser, 'iCount', $sUsername
          );
        },last SWITCH;

        $sOperation eq 'removeuser' and do {
          $bResult = 0;
          ($bResult, $osth_IsUser) =
            isuser(\$odbh, \$osth_IsUser,
                   $sSQL_Q_IsUser, 'iCount', $sUsername
          );
        },last SWITCH;

        $sOperation eq 'removeuser3' and do {
          $bResult = 0;
          $bResult = removeuser3(\$odbh, $sOperation, $sUsername);
        },last SWITCH;
      } # end SWITCH: removeuser3

      my $sOutput = pack "nn", 2, $bResult ? 1 : 0;
      syswrite(STDOUT, $sOutput);
      syslog(LOG_INFO, "PID: %d, request (%s => %s:%s:%s) (result => %d)",
             $$, $sOperation, $sUsername, $sDomain, $sPassword, $bResult);
    } # end while (1) for read STDIN
    $odbh->disconnect();
  }; # end eval
} # end global while(1)
closelog;
# vim: syntax=perl:fileencoding=utf-8:fileformat=unix:tw=78:ts=2:sw=2:sts=2:et
# EOF
